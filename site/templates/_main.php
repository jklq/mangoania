<?php namespace ProcessWire;

/**
 * _main.php
 * Main markup file
 *
 * This file contains all the main markup for the site and outputs the regions 
 * defined in the initialization (_init.php) file. These regions include: 
 * 
 *   $title: The page title/headline 
 *   $content: The markup that appears in the main content/body copy column
 *   $sidebar: The markup that appears in the sidebar column
 * 
 * Of course, you can add as many regions as you like, or choose not to use
 * them at all! This _init.php > [template].php > _main.php scheme is just
 * the methodology we chose to use in this particular site profile, and as you
 * dig deeper, you'll find many others ways to do the same thing. 
 * 
 * This file is automatically appended to all template files as a result of 
 * $config->appendTemplateFile = '_main.php'; in /site/config.php. 
 *
 * In any given template file, if you do not want this main markup file 
 * included, go in your admin to Setup > Templates > [some-template] > and 
 * click on the "Files" tab. Check the box to "Disable automatic append of
 * file _main.php". You would do this if you wanted to echo markup directly 
 * from your template file or if you were using a template file for some other
 * kind of output like an RSS feed or sitemap.xml, for example. 
 *
 * See the README.txt file for more information. 
 *
 */
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php echo $title; ?></title>
	<meta name="description" content="<?php echo $page->summary; ?>" />
	<link href='//fonts.googleapis.com/css?family=Lusitana:400,700|Quattrocento:400,700' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/tailwind.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" />

</head>
<body class="<?php if($sidebar) echo "has-sidebar "; ?>">


	<div class="bg-blue-600 grid grid-cols-12 shadow fixed w-full ">
		<!-- top navigation -->
		<ul role='navigation' class="mt-0 flex col-span-6"><?php

			$navAnchorClass = "text-white height-full py-2 px-3 block hover:bg-blue-500 hover:text-white duration-100 border-none";

			// top navigation consists of homepage and its visible children
			// <li><a href='$item->url'>$item->title</a></li>
			foreach($homepage->and($homepage->children) as $item) {
				echo "<li class='list-none m-0'";
				if($item->id == $page->rootParent->id) {
					echo "current' aria-current='true'>";
				} else {
					echo ">";
				}
				echo "<a class='$navAnchorClass' href='$item->url'>$item->title</a></li>";
			}

			?>

			
		</ul>

		<!-- search form-->
		<form class='search col-span-6' action='<?php echo $pages->get('template=search')->url; ?>' method='get'>
			<label for='search' class='visually-hidden'>Search:</label>
			<input type='text' name='q' placeholder='Search' id='search' value='<?php echo $sanitizer->entities($input->whitelist('q')); ?>' />
			<button type='submit' name='submit' class='visually-hidden'>Search</button>
		</form>
		
	</div>

	<div class="h-16"></div>
	<div class="container">
	<?php 
	// output an "EditEdit" link if this page happens to be editable by the current user
	if($page->editable()) echo "<a href='$page->editUrl'>Edit</a>"; ?>

	</div>
	<!-- breadcrumbs -->
	<div class='breadcrumbs container mb-3' role='navigation' aria-label='You are here:'><?php
		// breadcrumbs are the current page's parents
		foreach($page->parents() as $item) {
			echo "<span><a href='$item->url'>$item->title</a></span> "; 
		}
		// optionally output the current page as the last item
		//echo "<span>$page->title</span> "; 
	?></div>

	<div id='main' class="lg:grid grid-cols-12 container gap-16">

		<!-- main content -->
		<div id='content' class="<?php if($sidebar): ?>col-span-8<?php else: ?> col-span-12 <?php endif; ?> ">
			<h1><?php echo $title; ?></h1>
			<?php echo $content; ?>
		</div>

		<!-- sidebar content -->
		<?php if($sidebar): ?>
		<aside id='sidebar' class="col-span-3">
			<?php echo $sidebar; ?>
		</aside>
		<?php endif; ?>

	</div>

	<!-- footer -->
	<footer class="bg-red-600">
		<div class="container">
			<p>
			Powered by <a href='http://processwire.com'>ProcessWire CMS</a>  &nbsp; / &nbsp; 
			<?php 
			if($user->isLoggedin()) {
				// if user is logged in, show a logout link
				echo "<a href='{$config->urls->admin}login/logout/'>Logout ($user->name)</a>";
			} else {
				// if user not logged in, show a login link
				echo "<a href='{$config->urls->admin}'>Admin Login</a>";
			}
			?>
			</p>
		</div>
	</footer>

</body>
</html>
